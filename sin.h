#ifndef SIN_H
#define SIN_H

#include <stdint.h>

double GetSin(uint16_t);

#endif
