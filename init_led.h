#ifndef INIT_LED_H
#define INIT_LED_H

#include "stm32f4xx_gpio.h"

void init_led(void);

#endif
