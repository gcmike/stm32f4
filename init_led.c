#include "init_led.h"

void init_led(void){
	//***********************************
	// The following are for LEDs to work
	//***********************************
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
	
	//NVIC->ISER[0] |= (1 << (TIM3_IRQn));
	//NVIC->ISER[0] |= (1 << (TIM4_IRQn));
	//***********************************
}
/*
//***********************************************************
//The following interrupt functions are for LED blinking test
//***********************************************************
void TIM3_IRQHandler(void){
	if(TIM3->SR & TIM_SR_UIF) GPIOD->ODR ^= (1 << 12);
	TIM3->SR = 0;
}
void TIM4_IRQHandler(void){
	if(TIM4->SR & TIM_SR_UIF) GPIOD->ODR ^= (1 << 13);
	TIM4->SR = 0;
}
//***********************************************************
*/
