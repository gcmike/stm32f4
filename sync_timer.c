////////////////////////////////////////////////////////////////////
// This program sets Timer 3 and Timer 4 to act synchronously.
// Timer 4 is started by channel 4 of Timer 3 and channel 1, 2,
// and 3 of both timers generate PWM signals. The three outputs
// of Timer 3 are active-high, while that of Timer 4 are active-
// low.
//
// Function description
// --------------------
//
// SetSyncTimers:
//   Configure both timers and setup the timebase.
//   Output of PWMs are:
//   TIM3OC1 -> PB4
//   TIM3OC2 -> PB5
//   TIM3OC3 -> PB0
//   TIM4OC1 -> PD12
//   TIM4OC2 -> PD13
//   TIM4OC3 -> PD14
//   These could be reconfigured.
//
//   Parameter:
//   psc(0 ~ 65535)        The prescaler of timerbase
//
// SetPWMH:
//   Configure the three channels of Timer 3. Duty cycle is the
//   timer period of output-high.
//
//   Parameter:
//   arr(0 ~ 2^32 - 1)     The period of PWM
//   dty1(0 ~ 2^32 - 1)    The duty cycle of PWM 1
//   dty2(0 ~ 2^32 - 1)    The duty cycle of PWM 2
//   dty3(0 ~ 2^32 - 1)    The duty cycle of PWM 3
//
// SetPWML:
//   Configure the three channels of Timer 4. Outputs are set
//   to be active-low. Duty cycle is the timer period of output-low.
//
//   Parameter:
//   arr(0 ~ 2^32 - 1)     The period of PWM
//   dty1(0 ~ 2^32 - 1)    The duty cycle of PWM 1
//   dty2(0 ~ 2^32 - 1)    The duty cycle of PWM 2
//   dty3(0 ~ 2^32 - 1)    The duty cycle of PWM 3
//
// TimerStart:
//   Configure the timers to auto-reload, center-aligned and
//   enable the timers.
////////////////////////////////////////////////////////////////////

#include "sync_timer.h"

void SetSyncTimers(uint16_t psc){
	// Enable timer 3 and timer 4
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN | RCC_APB1ENR_TIM4EN;
	
	// Enable output ports
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN | RCC_AHB1ENR_GPIOBEN;
	
	// Set output pins, PB0, PB4, PB5, PD12, PD13, PD14
	GPIOB->AFR[0] = 0x220002;
	GPIOD->AFR[1] = 0x2220000;
	
	// Set selected pins to Alternate Function instead of GPIO
	GPIOB->MODER |= 0x0A02;
	GPIOD->MODER |= 0x2A000000;
	
	// OC4REF of timer 3 is used to trigger timer 4
	TIM3->CR2 |= 0x0070;
	
	// Set timer 4 to be internal triggered from timer 3, gated mode
	TIM4->SMCR |= 0x0025;
	
	// Set channels in PWM mode and auto-reload from arr, the period value
	// Channel 4 of timer 4 is not used
	TIM3->CCMR1 |= 0xF8F8;
	TIM4->CCMR1 |= 0xF8F8;
	TIM3->CCMR2 |= 0xF8F8;
	TIM4->CCMR2 |= 0x00F8;
	
	// Update generated, counters starting from the beginning
	TIM3->EGR = TIM_EGR_UG;
	TIM4->EGR = TIM_EGR_UG;
	
	// Set timer 3 to be active high, timer 4 to be active low
	// Output to pins
	TIM3->CCER = 0x0333;
	TIM4->CCER = 0x0333;
	
	// Timer prescale value
	TIM3->PSC = psc;
	TIM4->PSC = psc;
}

void SetPWM(uint32_t arr, uint32_t dty1, uint32_t dty2, uint32_t dty3){
	TIM3->ARR = arr;
	TIM4->ARR = arr;
	TIM3->CCR1 = arr - dty1;
	TIM3->CCR2 = arr - dty2;
	TIM3->CCR3 = arr - dty3;
	TIM4->CCR1 = arr - dty1 - 1;
	TIM4->CCR2 = arr - dty2 - 1;
	TIM4->CCR3 = arr - dty3 - 1;
}

void SetPWMH(uint32_t arr, uint32_t dty1, uint32_t dty2, uint32_t dty3){
	// Auto-reload value, period of PWM
	TIM3->ARR = arr;
	
	TIM3->CCR1 = arr - dty1;
  TIM3->CCR2 = arr - dty2;
	TIM3->CCR3 = arr - dty3;
}

void SetPWML(uint32_t arr, uint32_t dty1, uint32_t dty2, uint32_t dty3){
	// Auto-reload value, period of PWM
	TIM4->ARR = arr;
	
	TIM4->CCR1 = arr - dty1;
	TIM4->CCR2 = arr - dty2;
	TIM4->CCR3 = arr - dty3;
}

void TimerStart(void){
	TIM4->CR1 |= 0x00E1; // Auto-reload, Center-aligned, Counter enable
	TIM3->CR1 |= 0x00E1;
}
